# -*- encoding: utf-8
import sqlite3 as sql
import sys
import random
import items


PRESENT_RESULTS = 5
MAX_RATING_RADIUS = 10
NEW_RECOMMENDS = False


def sorter(func):
    def __inner__(alist):
        alist.sort(key=func)

    return __inner__

@sorter
def sort_by_score(it):
    return -it[1]

def userlist(cursor):
    return (x[0] for x in cursor.execute('SELECT DISTINCT RatingUserID FROM Ratings'))

def main(conn, my_user = None):
    # preinit
    cursor = conn.cursor()

    try:
        cursor.execute('CREATE TABLE Ratings (Rating REAL, ItemID TEXT, RatingUserID TEXT)')

    except sql.DatabaseError:
        pass

    # pick an user to recommend for
    my_user = my_user or random.choice(tuple(userlist(cursor)))

    # build results
    results = []

    if NEW_RECOMMENDS:
        recommendables = cursor.execute('SELECT ItemID AS item FROM Ratings WHERE NOT EXISTS (SELECT * FROM Ratings WHERE RatingUserID = ? AND ItemID = item LIMIT 1)', [my_user])

    else:
        recommendables = ((x,) for x in items.item_names)

    for (item,) in recommendables:
        new_item = items.find_item(item)
        result = []
        linked = []

        for (user, middle_rating) in cursor.execute("SELECT RatingUserID AS UserID, Rating FROM Ratings WHERE ItemID = ?", [item]):
            for (linked_item, linked_rating) in cursor.execute("SELECT ItemID, Rating FROM Ratings WHERE RatingUserID = ?", [user]):
                other_item = items.find_item(linked_item)
                cursor.execute('SELECT Rating FROM Ratings WHERE RatingUserID = ? AND ItemID = ?', [my_user, linked_item])
                rating = cursor.fetchone()

                if rating != None:
                    new_rate = (linked_rating * middle_rating * rating[0] / pow(MAX_RATING_RADIUS, 3) / (1 + items.item_distance(new_item, other_item))) * MAX_RATING_RADIUS
                    
                    result.append(new_rate)
                    linked.append((user, middle_rating, linked_item, linked_rating))
        
        if len(result) > 0:
            results.append((item, sum(result) / len(result), linked))

    sort_by_score(results)
    
    good_results = tuple((x for x in results if x[1] > 0))

    # present results
    print(f"Your name is {my_user}.")
    
    for (item, rating) in cursor.execute('SELECT ItemID, Rating FROM Ratings WHERE RatingUserID = ?', [my_user]):
        print(f"  • You give {item} {0.1 * int(10 * rating)} stars.")

    print()
    print(f"Therefore, you might (or not) like:")

    for (item, score, linked) in results[:PRESENT_RESULTS]:
        if score != 0:
            print("  • {item} (≃ {score} stars) - because {because}".format(
                item = item,
                score = 0.01 * int(100 * score),
                because = '; '.join(f"{l[0]} gave it {0.1 * int(0.1 * l[1])} stars, {'and' if (l[1] > 0) == (l[3] > 0) else 'but'} gave '{l[2]}' {0.1 * int(10 * l[3])} stars" for l in linked)
            ))

    # close database and exit
    conn.close()

    return 0


if __name__ == "__main__":
    sys.exit(main(sql.connect('ratings.db'), sys.argv[1] if len(sys.argv) > 1 else None))
