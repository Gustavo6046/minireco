# -*- encoding: utf-8
import sqlite3 as sql
import sys
import random
import namegen
import items


MAX_RATING_RADIUS = 10

items = items.item_names


def generate_ratings(num_ratings = 5):
    seq = random.sample(items, int(min(num_ratings, len(items))))

    for item in seq:
        yield (item, 1.0 * random.randint(-MAX_RATING_RADIUS * 10, MAX_RATING_RADIUS * 10) / 10)

def main(conn, num_people=1):
    cursor = conn.cursor()

    try:
        cursor.execute('CREATE TABLE Ratings (Rating REAL, ItemID TEXT, RatingUserID TEXT)')

    except sql.DatabaseError:
        pass

    for i in range(num_people):
        name = namegen.generate_name(random.randint(3, 7), hyphen_rate=0.05)

        print('    [{}]'.format(name))

        for (item, score) in generate_ratings(random.randint(2, 5) * len(items) / 6):
            print("{score}★ -> {item}".format(name=name, item=item, score=score))
            cursor.execute('INSERT INTO Ratings VALUES (?, ?, ?)', [score, item, name])

    conn.commit()
    conn.close()

    return 0


if __name__ == "__main__":
    sys.exit(main(sql.connect('ratings.db'), int(sys.argv[1]) if len(sys.argv) > 1 else 1))
